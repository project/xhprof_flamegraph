<?php
/**
 * @file
 * Admin configuration forms and functionality for XHProf Flamegraph module.
 */

/**
 * Form callback for XHProf Flamegraph creation form.
 */
function xhprof_flamegraph_generate_form($form, &$form_state) {
  $form['path_search'] = array(
    '#type' => 'textfield',
    '#title' => t('Path Search'),
    '#description' => t('Search XHProf samples by request path to generate a flamegraph'),
  );

  $form['generate'] = array(
    '#type' => 'submit',
    '#value' => t('Generate'),
  );

  if (!empty($form_state['storage']['flamegraph'])) {
    $form['#suffix'] = $form_state['storage']['flamegraph'];
  }

  return $form;
}

/**
 * Submit handler for flamegraph generation form.
 */
function xhprof_flamegraph_generate_form_submit(&$form, &$form_state) {
  $form_state['rebuild'] = TRUE;

  $runclass = xhprof_sample_runclass();
  $samples = $runclass::collectWhere('path', trim($form_state['values']['path_search']));

  if (empty($samples)) {
    drupal_set_message(t('No samples found for this path'), 'error');
    return;
  }

  $stacks = xhprof_flamegraph_samples_to_stacks($samples);
  if ($flamegraph = xhprof_flamegraph_stacks_to_flamegraph($stacks)) {
    $form_state['storage']['flamegraph'] = $flamegraph;
  }
}
