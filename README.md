# XHProf FlameGraph

Provides flame graph generation as an addon to the [XHProf Sample](https://www.drupal.org/project/xhprof_sample) module. 

Flame graphs are a visual mechanism for analyzing profiling information from an application. This module can be used to analyze stack call samples from the XHProf PHP extension, which is useful for quickly and proactively identifying performance issues in Drupal.

## Requirements

- [XHProf Sample](https://www.drupal.org/project/xhprof_sample)
- [XHProf](https://pecl.php.net/package/xhprof) PHP extension
- [FlameGraph library](https://github.com/brendangregg/FlameGraph) from [@brendangregg](https://twitter.com/brendangregg), which requires Perl.

## Usage

Once enabled, a new operation will be available on the XHProf Sample output list. This will allow you to generate a flame graph for an individual request.

Additionally, a new tab will be available on the XHProf Sample administration page where you can generate a flame graph for all requests for a specific path. 